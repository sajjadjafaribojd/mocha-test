/* jshint esversion:6 */
const expect=require('expect');
const rewire=require('rewire');

var app=rewire('./app');

describe('App',()=>{ 
    var db={
        saveUser:expect.createSpy()
    };

    app.__set__('db',db);


    it('should call the spy correctlry',()=>{// create spy and call it.
        var spy =expect.createSpy();
        spy('jabj',25);
        //expect(spy).toHaveBeenCalled(); //for mor info mjsckson expext gitub
        expect(spy).toHaveBeenCalledWith('jabj',25);

    });

    it('should call saveUser with user object,',()=>{
        var email='jabj@jabj.com';
        var passwd='123456';

        app.handelSignup(email,passwd);
        expect(db.saveUser).toHaveBeenCalledWith(email,passwd);
    });

});