/* jshint esversion:6 */
const  request=require('supertest');
const  expect=require('expect');

 var app=require('./server').app;

 it('simple express test hello world',(done)=>{
    request(app)
    .get('/')
    .expect((res)=>{
        expect(res.body).toInclude({
            error: 'Page not found.'
        });
       
    })
    .expect(404)
    .end(done);
 });

 it('should return my user object',(done)=>{
    request(app)
    .get('/users')
    .expect((res)=>{
        expect(res.body).toInclude({
            name:'sajjad',
            age:30
        });
    })
    .expect(200)
    .end(done);
 });