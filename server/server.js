/* jshint esversion:6 */
const express=require('express');

var app=express();

app.get('/',(req,res)=>{
    res.status(404).send({
        error: 'Page not found.'
    });
});

app.get('/users',(req,res)=>{
    res.status(200).send([{
        name:'sajjad',
        age:30
    },{
        name:'reza',
        age:29
    },{
        name:'ali',
        age:29
    }]);
});

//app.listen(3000); // simple mode

var port=3000;
app.listen(port,'localhost',(error)=>{  // perfect mode
     if(error){
        throw new Error(`Can not connect to${port}`);
    }else{
        console.log(`Express runnig on port: ${port}`);
    }
});

module.exports.app=app;