# mocha-test
Use mocha test for web server(express) &amp; database and etc.

For express test use superfast module.
For database test use rewire module.

1- Use this commnad for Watch and Auto Restart Tests.
nodemone --exec 'npm test'

2- Add this commnad to  package.json
"test-watch":"nodemon --exec 'npm test'"

3- For run top commnad write below commnad in terminal
npm run "test-watch"
