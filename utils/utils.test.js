/* jshint esversion:6 */
const expect=require('expect');

const utils=require("./utils");

it("Should add two number",()=>{
    var res=utils.add(11,22);

     expect(res).toBe(33).toBeA('number'); // value using ===
    //expect(res).toNotBe(44);


});

it('should square a number',()=>{
    var res=utils.square(5);

    expect(res).toBe(25);

 
});

it("should expext some value",()=>{
    //expect(12).toBe(12);
    //expect("JabJ").toBe("JabJ").toBeA('string');
    //expect({name:'jabj'}).toBe({name:'jabj'}); //error
    //expect({name:'jabj'}).toEqual({name:'jabj'}); //true

    //expect([2,3,"jabj"]).toInclude("jabj");
    expect({
        name:'sajjad',
        age:25
    }).toInclude({
        //age:26 //false
        age:25
    });

});

it("should set firstName and lastName",()=>{
    //var res=utils.setName({location:'iran', age:25},'sajjad jafari');
    var user={location:'iran', age:25};
    var res=utils.setName(user,'sajjad jafari');

    expect(res).toInclude({
        firstName:'sajjad',
        lastName:'jafari'
    });
});

//test async function;
it('should be async function sum',(done)=>{ //use done for async function because we have delay.
    utils.aSync(3,5,(sum)=>{
        expect(sum).toBe(8);
        done();//use done for async function because we have delay.
    });
}); 